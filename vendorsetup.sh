# ROM source patches

color="\033[0;32m"
end="\033[0m"

echo -e "${color}Applying patches${end}"
sleep 1


# Clone kernel
git clone https://gitlab.com/Juiceoss/kernel-xiaomi-chime.git -b pyro kernel/xiaomi/chime

# Clone vendor
git clone https://gitlab.com/hac4us06/vendor-xiaomi-chime-13 -b lineage-20 vendor/xiaomi/chime
git clone https://gitlab.com/hac4us06/vendor-xiaomi-citrus-13 -b lineage-20 vendor/xiaomi/citrus
git clone https://gitlab.com/hac4us06/vendor-xiaomi-lime-13 -b lineage-20 vendor/xiaomi/lime

# Hardware
git clone https://github.com/hac4us06/hardware_xiaomi13 -b thirteen hardware/xiaomi
